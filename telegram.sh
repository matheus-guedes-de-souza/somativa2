#!/bin/sh

# Defina o token do bot e o ID do chat como variáveis de ambiente
BOT_TOKEN="7112524264:AAGJvGcuK5ZGJb3LMA4uCgnsdA8ABN6VVBY"
CHAT_ID="5093573788"  # Substitua por seu ID de chat do Telegram

# URL do bot Telegram
BOT_URL="https://api.telegram.org/bot${BOT_TOKEN}/sendMessage"

# Formato da mensagem
PARSE_MODE="Markdown"

# Verifique o status do trabalho do GitLab
if [ "$CI_JOB_STATUS" = "success" ]; then
    STATUS="succeeded"
else
    STATUS="failed"
fi

# Construa a mensagem
MESSAGE="
-------------------------------------
GitLab build *${STATUS}*!
\`Repository:  ${CI_PROJECT_DIR}\`
\`Branch:      ${CI_COMMIT_BRANCH}\`
*Commit Msg:*
${CI_COMMIT_MESSAGE}
--------------------------------------
"

# Enviar a mensagem via curl
curl -s -X POST ${BOT_URL} -d chat_id=${CHAT_ID} -d text="${MESSAGE}" -d parse_mode=${PARSE_MODE}
