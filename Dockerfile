FROM ubuntu:latest
RUN mkdir /app
WORKDIR /app
RUN apt-get update
RUN apt-get install -y python3
COPY calculadora.html /app/calculadora.html
CMD ["python3", "-m", "http.server", "8080"]